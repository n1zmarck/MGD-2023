using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace itemPlayerObject
{


    public class StateMachine : MonoBehaviour
    {

        State currentstate, nextstate;

        // Start is called before the first frame update
        void Start()
        {
            currentstate = GetComponent<State>();
            currentstate = new idle();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }


    public class State : MonoBehaviour
    {
        public virtual void RunthisFunction(Vector3 Target)
        {
            
        }
    }

    public class Run : State
    {
        public override void RunthisFunction(Vector3 Target)
        {

        }
    }

    public class Swipe : State
    {
		public override void RunthisFunction(Vector3 Target)
		{

		}
	}

    public class idle : State
    {
		public override void RunthisFunction(Vector3 Target)
		{
			
		}
	}


}