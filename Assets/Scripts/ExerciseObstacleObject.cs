namespace Ex5CodeBase
{


    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class ExerciseObstacleObject : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            gameObject.transform.position += new Vector3(3, 0, 0) * Time.deltaTime;
        }


		private void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.tag == "Wall")
            {
                Destroy(this);

            }
		}
	}
}