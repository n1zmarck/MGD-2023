using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{

    public Button button;
    public Button button2;
    //public Material SphereMat;
    public List< Material> materials;
    public List<GameObject > obj;
    public GameObject prefab;


	private void Start()
	{
        //all objects have no material by default but they do have a renderer.
		foreach (GameObject item in obj)
        {
            //create temporary renderer
            Renderer temprend;
            //access to gameobjects renderer and reference it.
            temprend = item.GetComponent<Renderer>();
            //since object does not have material set. create one instead with "Standard Shader". can be changed with keywords.
            temprend.material = new Material(Shader.Find("Standard"));

            materials.Add(temprend.material);
            
            
        }
    }
	// Update is called once per frame
	void Update()
    {

		//add hook to changecolor()
		button.onClick.AddListener(changeColor);
		button2.onClick.AddListener(createNewObj);
	}
    void createNewObj()
    {
        GameObject temp = Instantiate(prefab, new Vector3(getNum(), getNum(), getNum()), Quaternion.identity);
        createNewMat(temp);
		obj.Add(temp);
        materials.Add(temp.GetComponent<Renderer>().material);

    }

    void createNewMat(GameObject gb )
    {   
		//create temporary renderer
		Renderer temprend;
		//access to gameobjects renderer and reference it.
		temprend = gb.GetComponent<Renderer>();
		//since object does not have material set. create one instead with "Standard Shader". can be changed with keywords.
		temprend.material = new Material(Shader.Find("Standard"));

	}

	private void changeColor()
    {
        foreach(Material mat in materials)
        {
            // following the Dry Kiss methodology, move random.range to new function. color cannot be changed directly but can be changed as a new instance.
			mat.color = new Color(getNum(), getNum(), getNum());
            // property to id needs to follow the property string in their shader file. this is case specific.
			mat.SetFloat(Shader.PropertyToID("_Metallic"), getNum());
			mat.SetFloat(Shader.PropertyToID("_Glossiness"), getNum());
		}



      
    }

    private float getNum()
    {
        return Random.RandomRange(0.00f, 1.00f);
    }
}
