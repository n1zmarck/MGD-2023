using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
public class InputScript : MonoBehaviour
{
    //TouchPhase phase;
    Vector2 Sposition, Eposition;
    public Rigidbody body;
    public Canvas canvas;
    //public GameObject body;
    public Vector3 SpawnPosition;
    [SerializeField] float Thrust;
    public GameUtils gameUtils;



    //rigidbody will be pushed with force without slowing down until hits collider.

    void calculateVector()
    {
        Vector2 deltaPosition = Eposition.normalized - Sposition.normalized;
        Debug.Log( string.Format("deltavector at {0} , {1}",  deltaPosition.x , deltaPosition.y));
      //  Vector2 RealDeltaPosition = Eposition - Sposition;
        
        if(deltaPosition.x > deltaPosition.y)
        {
            //X has moved more than  Y

            if (Eposition.x > Sposition.x)
            {
                //moved to the right
               
                body.AddForce(Vector3.right * Thrust);
				//body.transform.Translate(Vector3.forward * Thrust * Time.deltaTime);

				Debug.Log("Right");

                //interaction was right
            }
            else if (Eposition.x < Sposition.x)
            {
				//moved to the left

				body.AddForce(Vector3.back * Thrust);
				//body.transform.Translate(Vector3.back * Thrust * Time.deltaTime);

				Debug.Log("down");

                //interaction was down
			}

		}
        else
        {
            ///moved more in y axis
            
            if (Eposition.y > Sposition.y)
            {


				body.AddForce(Vector3.forward * Thrust);
				//body.transform.Translate(Vector3.right * Thrust * Time.deltaTime);

				Debug.Log("Up");
                //interaction was up

			}
			else if(Eposition.y < Sposition.y)
            {

				body.AddForce(Vector3.left * Thrust);
			//	body.transform.Translate(Vector3.left * Thrust * Time.deltaTime);

				Debug.Log("left");

                //interaction was left

			}

		}

    }


	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag == "Wall")
        {
            //body.velocity = Vector3.zero;
        }else if(collision.gameObject.tag == "Goal")
        {
            body.velocity = Vector3.zero;

            // Application.Quit();
            // spawn UI emmidiately 
            gameUtils.gameEnded();
        }else if (collision.gameObject.tag == "Hazard")
        {
            transform.position = SpawnPosition;
        }
	}

	// Update is called once per frame
	void Update()
    {

        if (Input.touchCount > 0)
        {
           Touch touch =  Input.GetTouch(0);

           //Vector3 TouchPosition = Camera.main.ScreenToWorldPoint(touch.position);

           // TouchPosition.z = 0;


			if (touch.phase == TouchPhase.Began)
			{
				Sposition = Input.GetTouch(0).position;
				Debug.Log("Touch Detected");
                //touch was detected.

			}
			else if (touch.phase == TouchPhase.Ended)
			{
				Eposition = Input.GetTouch(0).position;
				Debug.Log("Touch Lift");
                Debug.DrawLine(Sposition, Eposition,Color.cyan);
				calculateVector();
			    //lift was detected.
                
            }





		}




      
    }
}
