using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{

    public GameObject PlayerDestination;
    public NavMeshAgent agent;
    public GameObject ShootingPoint;
    public GameObject bulletPrefab;
    public AudioSource bulletSound;
    public GameUtils gameUtils;


    [SerializeField]
    int pointsWorth { get; set; }
    [SerializeField]
    Color defColor = Color.white;
    // Start is called before the first frame update
    [SerializeField]
    float ShootDelay = 2f;
    //delay in seconds;

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(ShootDelay);
        Instantiate(bulletPrefab,gameObject.transform);
        bulletSound.Play();
        yield return null;

    }

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();

		if (pointsWorth > 2) { gameObject.GetComponent<Material>().color = new Color(255, 215, 0); }
		else { gameObject.GetComponent<Material>().color = defColor; }
        gameUtils = gameUtils.GetInstance();

	}

	// Update is called once per frame
	void Update()
    {
        while (gameObject.active == true)
        {
			agent.destination = PlayerDestination.transform.position;
            StartCoroutine(Shoot());
		}
	}


	private void OnCollisionEnter(Collision collision)
	{
        if (collision.transform.tag == "Player" || collision.transform.tag == "PlayerBullet") 
        {
            gameUtils.IncrementScore(pointsWorth);
            DestroyImmediate(gameObject);
        }
	}
}
