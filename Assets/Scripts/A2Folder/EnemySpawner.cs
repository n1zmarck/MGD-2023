using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    EnemySpawner Instance;

    [SerializeField]
    float enemySpeed, obstacleSpeed;
    [SerializeField]
    GameObject enemyPrefab;
    [SerializeField]
    GameObject obstaclePrefab;

    [SerializeField]
    GameObject[] EnemySpawnLocation, ObstacleSpawnLocation;



    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }


    void spawnNewEnemy(int Location)
    {
        Instantiate(enemyPrefab, EnemySpawnLocation[Location].transform);


    }

    void spawnNewObstacle(int Location)
    {
        Instantiate(obstaclePrefab, ObstacleSpawnLocation[Location].transform);
    }

    // Update is called once per frame
    void Update()
    {

        int temp = 0;

        temp = Random.Range(1, 100);


        if(temp%2 == 0)
        {
            spawnNewEnemy(temp % 4);
        }else
        {
            spawnNewObstacle(Random.Range(0, 1));

        }



    }
}
