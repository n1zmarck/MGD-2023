using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.ProBuilder.MeshOperations;

public class PlayerScript : MonoBehaviour
{

	PlayerScript instance;


	//public GameObject body;
	public Vector3 SpawnPosition;
	[SerializeField] float Thrust;
	[SerializeField] int PlayerLives;
	public GameUtils gameUtils;
	public float ShootDelay = 0.5f;
	[SerializeField] GameObject bulletPrefab;
	[SerializeField] AudioSource PewShoot;
	Touch touch;

	private void Awake()
	{
		PlayerLives = PlayerPrefs.GetInt("PlayerLives", 3);
	}
	private void Start()
	{
		if (instance == null) { instance = this; }

		if (this.tag != "Player") { this.tag = "Player"; }
	}

	void Shooting()
	{
		
		Instantiate(bulletPrefab);
		PewShoot.Play();

	}

	void HealthUpAndDown(int Value)
	{

		PlayerLives += Value;
		gameUtils.HealthUpdate(PlayerLives);
	}

	public void RestartGame()
	{
		PlayerLives = 3;
		gameUtils.Respawn();
		gameUtils.resetScore();
		gameObject.transform.position = gameUtils.SpawnPoint;
	}

    // Update is called once per frame
    void Update()
    {

		if ( Input.touchCount > 0 )
		{

			touch = Input.GetTouch(0);
			while (Input.touchCount > 0 && touch.phase != TouchPhase.Ended)
			{


				gameObject.transform.position = Camera.main.ScreenToWorldPoint(touch.position);


				InvokeRepeating("Shooting", 0, ShootDelay);


			}

		}


		if (PlayerLives ==0)
		{
			gameUtils.gameEnded();
		}
	}


	private void OnCollisionEnter(Collision collision)
	{
		if (collision.transform.tag == "EnemyBullet" || collision.transform.tag == "Obstacle" || collision.transform.tag == "Enemy")
		{
			HealthUpAndDown(-1);
		}else if (collision.transform.tag == "HealthPickUp")
		{
			HealthUpAndDown(1);
		}
	}

}
