using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour
{


/* Unmerged change from project 'Assembly-CSharp.Player'
Before:
    public AudioSource audio;
After:
	private AudioSource audio;
*/
	private AudioSource audio;

	public AudioSource Audio { get => audio; set => audio = value; }

	// Start is called before the first frame update
	void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
		collision.gameObject.SetActive(false);
        Audio.Play();
	}

}
