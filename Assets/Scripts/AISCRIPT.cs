using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AISCRIPT : MonoBehaviour
{
    [SerializeField] private Transform MovePositionTransform;
    private NavMeshAgent agent;
    float tempx, tempy ;
    float sensitivity = 0.8f;

    [SerializeField]public Camera main;
    // Start is called before the first frame update
    void Start()
    {
        
    }

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
        //main = GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update()
    {
        agent.SetDestination(MovePositionTransform.position);

        main.transform.localRotation = Quaternion.Euler(Mathf.Clamp( (tempx += Input.GetAxis("Mouse Y")*sensitivity),-90f,90f),Mathf.Clamp( (tempy += Input.GetAxis("Mouse X") * sensitivity),-90f,90f), 0);
        
    }
}
