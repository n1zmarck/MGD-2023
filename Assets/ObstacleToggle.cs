using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleToggle : MonoBehaviour
{

    public GameObject obstacle;
    


	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag == "Player" )
        {
           // obstacle.SetActive(false);
            Destroy(obstacle);

            Destroy(gameObject, 0.05f);
        }
	}
}
