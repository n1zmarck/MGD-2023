using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{

    [SerializeField]
    GameObject sphereObject;
    [SerializeField]
    float Speed = 4;
    [SerializeField]
    Canvas GameWin;
    [SerializeField]
    Canvas GameLose;
    [SerializeField]
    Transform SpawnBeacon;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

		if (Input.GetKey(KeyCode.W)) { sphereObject.transform.position += Vector3.forward * Speed * Time.deltaTime; }
		else 
        if(Input.GetKey(KeyCode.A)) { sphereObject.transform.position += Vector3.back*Speed *Time.deltaTime;}
		else
		if (Input.GetKey(KeyCode.S)) { sphereObject.transform.position += Vector3.left * Speed * Time.deltaTime; }
		else
		if (Input.GetKey(KeyCode.D)) { sphereObject.transform.position += Vector3.right * Speed * Time.deltaTime; }
        else
        if (Input.GetKey(KeyCode.R))
        {
            RestartGame();
        }
		

	}

	private void RestartGame()
	{
		gameObject.transform.position = SpawnBeacon.transform.position;
        GameLose.gameObject.SetActive(false);
        GameWin.gameObject.SetActive(false);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Wall") { 
            GameLose.gameObject.SetActive(true);

        } 
        else if (collision.gameObject.tag == "Goal") {
            
            GameWin.gameObject.SetActive(true);

		}
	}
}
