using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField]
    float Speed = 20f;

    // Update is called once per frame
    void Update()
    {
		gameObject.transform.Translate(Vector3.forward * Speed * Time.deltaTime, Space.Self);

	}

	private void OnCollisionEnter(Collision collision)
	{
		Destroy(gameObject);
	}

}
