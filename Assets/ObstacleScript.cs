using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(Vector3.forward * 50 * Time.deltaTime, Space.Self);
    }
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.transform.tag == "Wall") { Destroy(gameObject); }
	}
}
