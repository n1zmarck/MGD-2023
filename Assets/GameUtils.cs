using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameUtils : MonoBehaviour
{

    public GameUtils Instance;

    public GameObject Player;
    public Canvas GameUIcanvas, EndScreenUICanvas;
    public TextMeshProUGUI PlayerHealthText, PlayerScoreText , PlayerScoreText2;
    public int PlayerScore;

    private bool gamePaused = false;
    public Vector3 SpawnPoint;


	public void Awake()
	{
		if (Instance == null)
            Instance = this;
	}


    public GameUtils GetInstance()
    {
        return this.Instance;
    }

	public void Respawn()
    {
        Player.transform.position = SpawnPoint;
        EndScreenUICanvas.gameObject.SetActive(false);
        GameUIcanvas.gameObject.SetActive(true);

    }

    public void quit()
    {
        Application.Quit();
    }

    public void gameEnded()
    {
        GameUIcanvas.gameObject.SetActive(false);
        EndScreenUICanvas.gameObject.SetActive(true);
        pauseGame();
	}

    public void pauseGame()
    {
        if (gamePaused == true) {
        Time.timeScale = 1.0f;
        }else { Time.timeScale = 0.0f; }

    }

    public void IncrementScore(int Value)
    {
        PlayerScore += Value;
        PlayerScoreText.text = string.Format("Score   {0}", PlayerScore);
        PlayerScoreText2.text = string.Format("Score   {0}", PlayerScore);
    }

    public void HealthUpdate(int Value)
    {
        PlayerHealthText.text = string.Format("Health   {0}", Value);

	}

    public void resetScore()
    {
        PlayerHealthText.text = string.Format("Health   {0}", PlayerPrefs.GetInt("PlayerHealth",3));
		PlayerScoreText.text = string.Format("Score   {0}", 0);
		PlayerScoreText2.text = string.Format("Score   {0}", 0);



	}

	private void OnApplicationQuit()
	{
        PlayerPrefs.Save();

	}

	private void OnApplicationFocus(bool focus)
	{
		if (focus == false) {
            Time.timeScale = 0.0f;
        
        }else if (focus == true) { Time.timeScale = 1.0f;}
	}

}
